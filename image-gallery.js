// Allow us to use the Express framework
var express = require('express');

// TODO Ex 4 Step 1. Specify that the app should use fs (to scan directory contents)
var fs = require("fs");

// TODO Ex 5 Step 2. Specify that the app should use Formidable (to process file uploads) and Jimp (to create image thumbnails).
var formidable = require('formidable');
var jimp = require("jimp");
// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Reads all the images in the public/images/thumbnails folder, then
// renders the image-gallery/image-gallery view.

var outputFolder = __dirname + "/public/images/thumbnails/"; 
var inputFolder = __dirname + "/public/images/fullsize/"; 
var thumbWidth = 400;
var thumbHeight = 400;


//Specify that when we browse to "/" with a GET request
// app.get('/', function (req, res) {
//     //res.render("image-gallery/image-gallery");
// });    

function renderImageGallery(req, res) {

    console.log("start of renderImageGallery function");

    // TODO Ex 4 Steps 2 through 4.
    fs.readdir(inputFolder, function(err, files){
        console.log(files.length + "files found in " + inputFolder);

       var gallery = [];

        for(var i= 0; i < files.length; i++) {
            var file = files[i].toLowerCase();

            if(file.endsWith(".jpg") || file.endsWith(".bmp") ||
            file.endsWith(".jpeg") || file.endsWith(".png")) {
                var inputFileName = inputFolder + file;
                var outputFileName = outputFolder + file;
                 generateThumbnail(inputFileName, outputFileName);
                gallery[i] = file;
            }
        }

        console.log(inputFileName);

        var data = {
            pageTitle: "Image Gallery",
            gallery: gallery
    }
        
        res.render("image-gallery/image-gallery", data);
    });
}

// This function will (asynchronously) generate a thumbnail for the given input file, and save it to the given output file.
function generateThumbnail(inputFileName, outputFileName) {
    console.log("Reading " + inputFileName + "...");
    jimp.read(inputFileName, function (err, image) {

        // When the image is loaded in Jimp, scale the image to fit
        // the desired size and then save it to the output folder.
        image
            .scaleToFit(thumbWidth, thumbHeight)
            .write(outputFileName, function (err) {
                // When the image is done saving, print this message to the console
                if (err) {
                    console.error("Error saving file " + outputFileName + "!");
                } else {
                    console.log(outputFileName + " saved successfully!");
                }
            });
    })
}

// Specify that when we browse to "/" with a GET request, render the image gallery.
app.get('/', function (req, res) {
    // console.log(req);
    console.log("hello")
     renderImageGallery(req, res);
    
});


// TODO Ex 5 Steps 3 through 7. Process the file upload, generate thumbnail, display gallery.
app.post('/', function(req, res) {
    var form = new formidable.IncomingForm();

    form.on("fileBegin", function(name, file){
        file.path = inputFolder + file.name;
        //file.path = __dirname + "/public/images/fullsize" + file.name;
    });

    form.parse(req, function(err, field, files) {
        console.log("form parse function should start");
        renderImageGallery(req, res);

    });

});

// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});